/*
###################################################
Arduino 5050 LED strip light controller program
Designed with IRFZ44N MOSFET's / Power Transistors
Code written by: Peter Stevenson (2E0PGS)
Intended for use with Arduino UNO
###################################################

Copyright (C) <2015>  <Peter Stevenson> (2E0PGS)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

//-----------------------ETHERNET-SETTINGS-----------------------//
#include <SPI.h>
#include <Ethernet.h>
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0x27 };
byte ip[] = { 192, 168, 0, 20 };
EthernetServer server(80);  // create a server at port 80
/*
Pins used
SPI Bus via ISP header.
This is on digital pins 10, 11, 12, and 13 on the Uno.
Pin 4 for the SD card.
*/
String HTTP_req; // stores the HTTP request
boolean PowerState = 1;
//----------END----------ETHERNET-SETTINGS----------END----------//

int red = 3; //red channel is pin 3. RED LED STRIP OUTPUT
int green = 5; //green channel is pin 5. GREEN LED STRIP OUTPUT
int blue = 6; //blue channel is pin 6. BLUE LED STRIP OUTPUT

int redanalogsensor = 1; //red analog pot input pin.
int greenanalogsensor = 2; //green analog pot input pin.
int blueanalogsensor = 3; //blue analog pot input pin.

int redbrightnessread = 0; //The varible that stores value of the RED analog input pin 0 - 1024.
int greenbrightnessread = 0; //The varible that stores value of the GREEN analog input pin 0 - 1024.
int bluebrightnessread = 0;  //The varible that stores value of the BLUE analog input pin 0 - 1024.

int timer = 1; //Timer used for delays in subroutines.
int timerpin = 4; //Pin used for timer input via POT or rotary encoder.
int timerpinread = 0; //The varible that stores the read value from the TIMER analog input pin 0 - 1024.
int timer_map_profile = 1; //Used for diffrent timer mapping profies.

boolean debugmode = 0; //used for enabling or disabling debug mode via serial USB.
int mode = 1; //The variable that controls the mode we are in.

int brightness = 100; //master brightness for general brightness use.

int LDRpin = 0; //LDR (light dependant resistor) pin input.
int LDRbrightness = 0; //LDR brightness for use in autoLDR function.
int LDRbrightnessread = 0; //The varible that stores value of the LDR analog input pin 0 - 1024.

int redbrightness = 0; //sub routine specific brightness control
int greenbrightness = 0; //sub routine specific brightness control
int bluebrightness = 0; //sub routine specific brightness control

void setup() {  
  pinMode(11, OUTPUT); //red channel is pin 11
  pinMode(10, OUTPUT); //green channel is pin 10
  pinMode(9, OUTPUT); //blue channel is pin 9

  Serial.begin(9600); //start serial connection
  Ethernet.begin(mac, ip); // initialize Ethernet device
  server.begin(); // start to listen for clients  
}


void jump3() { //jump 3 sequence. RED then GREEN then BLUE with 0 LOW time.
  timer_map_profile = 2;
  TimerPOT();
  digitalWrite(red, HIGH);   //red
  delay(timer);              
  digitalWrite(red, LOW);  
  
  TimerPOT();
  digitalWrite(green, HIGH);   //green
  delay(timer);              
  digitalWrite(green, LOW);   
  
  TimerPOT(); 
  digitalWrite(blue, HIGH);   //blue
  delay(timer);              
  digitalWrite(blue, LOW);    
}

void jump7() { //jump 7 sequence. RED then GREEN then BLUE then YELLOW then PURPLE then TURQUOISE then WHITE.
  timer_map_profile = 2;
  TimerPOT();
  digitalWrite(red, HIGH);   //red
  delay(timer);              
  digitalWrite(red, LOW);
  
  TimerPOT();  
  digitalWrite(green, HIGH);   //green
  delay(timer);              
  digitalWrite(green, LOW);      
  
  TimerPOT();
  digitalWrite(blue, HIGH);   //blue
  delay(timer);              
  digitalWrite(blue, LOW);    
  
  TimerPOT();
  analogWrite(red, 127); //yellow
  analogWrite(green, 127);
  delay(timer);
  digitalWrite(red, LOW);
  digitalWrite(green, LOW);
  
  TimerPOT();
  analogWrite(red, 127); //purple
  analogWrite(blue, 127);
  delay(timer);
  digitalWrite(red, LOW);
  digitalWrite(blue, LOW);
  
  TimerPOT();
  analogWrite(blue, 225); //turquoise
  analogWrite(green, 110);
  delay(timer);
  digitalWrite(blue, LOW);
  digitalWrite(green, LOW);
  
  TimerPOT();
  digitalWrite(red, HIGH); //white
  digitalWrite(green, HIGH);
  digitalWrite(blue, HIGH);
  delay(timer);
  digitalWrite(red, LOW); 
  digitalWrite(green, LOW);
  digitalWrite(blue, LOW);
  
}

void fade3() {  //fades red, green , blue
timer_map_profile = 1;
if (brightness < 255){ //used to set brightness to 0 on sequence start
 brightness = 0;
 }
 TimerPOT();
 while (redbrightness < 255){
   analogWrite(red, redbrightness);
   redbrightness++;
   delay(timer);
 }
 TimerPOT();
 while (bluebrightness > 0){
   analogWrite(blue, bluebrightness);
   bluebrightness--;
   delay(timer);
 }
 TimerPOT();
 while (greenbrightness < 255){
   analogWrite(green, greenbrightness);
   greenbrightness++;
   delay(timer);
 }
 TimerPOT();
 while (redbrightness > 0){
   analogWrite(red, redbrightness);
   redbrightness--;
   delay(timer);
 }
 TimerPOT();
 while (bluebrightness < 255){
   analogWrite(blue, bluebrightness);
   bluebrightness++;
   delay(timer);
 }
 TimerPOT();
 while (greenbrightness > 0){
   analogWrite(green, greenbrightness);
   greenbrightness--;
   delay(timer);
 } 

}

void fadeW() { //fade from 0 to 255 and then 255 to 0 in colour white.
 TimerPOT();
 timer_map_profile = 1;
 if (brightness < 255){ //used to set brightness to 0 on sequence start
 brightness = 0;
 }
 while (brightness < 255){
 analogWrite(red, brightness);
 analogWrite(green, brightness);
 analogWrite(blue, brightness);
 delay(timer);
 brightness++;
 }
 while (brightness > 0){ 
 analogWrite(red, brightness);
 analogWrite(green, brightness);
 analogWrite(blue, brightness);
 delay(timer);
 brightness--;
 }   
}

void colourred() { //plain RED colour
  analogWrite(red, brightness);
}
void colourgreen() { //plain green colour
  analogWrite(green, brightness);
}
void colourblue() { //plain blue colour
  analogWrite(blue, brightness);
}
void white() { //plain white colour
  analogWrite(red, brightness);
  analogWrite(green, brightness);
  analogWrite(blue, brightness);
}

void makeyourown() {
 redbrightnessread = analogRead(redanalogsensor); //set red brightness variable to the input value of red POT
 greenbrightnessread = analogRead(greenanalogsensor); //set green brightness variable to the input value of green POT
 bluebrightnessread = analogRead(blueanalogsensor); //set blue brightness variable to the input value of blue POT
 redbrightness = map(redbrightnessread, 0, 1023, 1, 255); //maps the red led input analog pin read of 0 - 1024 to a output brightness of 0 - 255.
 greenbrightness = map(greenbrightnessread, 0, 1023, 1, 255); 
 bluebrightness = map(bluebrightnessread, 0, 1023, 1, 255);  
 analogWrite(red, redbrightness); //set output color to that of variable
 analogWrite(green, greenbrightness); //set output color to that of variable
 analogWrite(blue, bluebrightness); //set output color to that of variable
}

void autoLDR() {
 LDRbrightnessread = analogRead(LDRpin);  
 
 if(LDRbrightnessread > 70){
 redbrightness = map(LDRbrightnessread, 0, 1023, 1, 255); //maps the red led input analog pin read of 0 - 1024 to a output brightness of 0 - 255.
 greenbrightness = map(LDRbrightnessread, 0, 1023, 1, 255);
 bluebrightness = map(LDRbrightnessread, 0, 1023, 1, 255); 
 analogWrite(red, redbrightness); //set output color to that of variable
 analogWrite(green, greenbrightness); //set output color to that of variable
 analogWrite(blue, bluebrightness); //set output color to that of variable
 }
 
 if(LDRbrightnessread < 70){ //if LDR reads a low brightness level then go night mode.
   digitalWrite(blue, LOW); //night mode colours
   analogWrite(red, 30);
   analogWrite(green, 2);
 }
}

void TimerPOT(){ //maps the 10k timer pot 
  timerpinread = analogRead(timerpin);
  if (timer_map_profile == 1){
  timer = map(timerpinread, 0, 500, 1, 50); //timer profile 1
  }
  if (timer_map_profile == 2){
  timer = map(timerpinread, 0, 500, 1, 10000); //timer profile 2
  }
  if (timer_map_profile == 3){
  timer = map(timerpinread, 0, 500, 1, 100); //not used yet
  }
  if (debugmode == 1){
  Serial.print("timer pin read ");
  Serial.println(timerpinread);
  Serial.print("timer ");
  Serial.println(timer);
  }
  
}


void loop(){ //which subroutine do we want to run ? This is the master loop. 
//later we can add a if statement for firealarm interupts etc. 
if (PowerState == 1){
  if (mode == 1){
  autoLDR();
  }
  if (mode == 2){
  jump3();
  }
  if (mode == 3){
  jump7();
  }
  if (mode == 4){
  fade3();
  }
  if (mode == 5){
  fadeW();
  }
}
else{
//if power state is 0 run no code.
analogWrite(red, 0);
analogWrite(green, 0);
analogWrite(blue, 0);
}

//--------------------HTTP SERVER CODE FOR WEBPAGE--------------------//
// Create a client connection
  EthernetClient client = server.available();
  if (client) {
    while (client.connected()) {   
      if (client.available()) {
        char c = client.read();
     
        //read char by char HTTP request
        if (HTTP_req.length() < 100) {
          //store characters to string
          HTTP_req += c;
          //Serial.print(c);
         }

         //if HTTP request has ended
         if (c == '\n') {          
           Serial.println(HTTP_req); //print to serial monitor for debuging
           client.println("HTTP/1.1 200 OK");
           client.println("Content-Type: text/html");
           client.println("Connection: close");
           client.println();
           // send web page
           client.println("<!DOCTYPE html>");
           client.println("<html>");
           client.println("<head>");
           client.println("<title>Arduino LED Lights Control</title>");
           client.println("</head>");
           client.println("<body>");
           client.println("<h1>Arduino LED Light Strip Controller 2E0PGS Shack</h1>");
           client.println("<p>Click buttons to control the lights</p>");
           client.println("<form method=\"get\">");
           client.println("<a href=\"/?button1on\"\">Turn ON</a>");
           client.println("<a href=\"/?button1off\"\">Turn OFF</a><br />");
           client.println("<a href=\"/?button2on\"\">AUTO LDR</a><br />");
           client.println("<a href=\"/?button3on\"\">Jump 3</a><br />"); 
           client.println("<a href=\"/?button4on\"\">Jump 7</a><br />");
           client.println("<a href=\"/?button5on\"\">Fade 3</a><br />"); 
           client.println("<a href=\"/?button6on\"\">Fade White</a><br />");                                       
           client.println("</form>");
           client.println("</body>");
           client.println("</html>");           
           Serial.print(HTTP_req);
           
           delay(1); 
           
           client.stop();
           
           if (HTTP_req.indexOf("?button1on") >0){
           PowerState = 1;
           }
           if (HTTP_req.indexOf("?button1off") >0){
           PowerState = 0;
           }
           if (HTTP_req.indexOf("?button2on") >0){
           mode = 1;
           }
           if (HTTP_req.indexOf("?button3on") >0){
           mode = 2;
           }
           if (HTTP_req.indexOf("?button4on") >0){
           mode = 3;
           }
           if (HTTP_req.indexOf("?button5on") >0){
           mode = 4;
           }
           if (HTTP_req.indexOf("?button6on") >0){
           mode = 5;
           }
           //clearing string for next read
           HTTP_req=""; 
         }
       } //END of client available
     } //END of client connected
   } //END of if client
   
 }//END OF VOID LOOP
