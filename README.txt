#############################################################
Arduino L.E.D 5050 RGB Strip Light Controller Program

Features:
*Auto LDR Dimming
*Fade Modes
*Jump R,G,B Modes
*Ethernet Control 
*Speed Control of effects
*Multiple Effects 

Arduino Boards:
*Designed and tested on Uno
*Should work with: Mega, Nano, Leonardo, Due

Librarys Used:
*<SPI.h>
*<Ethernet.h>

Licence:
*This program is licensed under GNU General Public Licence
*http://opensource.org/licenses/GPL-3.0

#############################################################